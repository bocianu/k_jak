const 
boat0_s: array [0..29] of byte =
  (
    $08, $08, $1c, $1c, $3c, $3e, $3e, $3e, 
    $3e, $3e, $3e, $77, $63, $41, $49, $5d, 
    $5d, $7f, $7f, $5d, $63, $36, $3e, $3e, 
    $3e, $3e, $3e, $3c, $1c, $1c
  );
boat0_l: array [0..29] of byte =
  (
    $10, $10, $18, $38, $38, $3c, $3c, $3c, 
    $7e, $7e, $7e, $7f, $67, $63, $41, $5d, 
    $5f, $5f, $5d, $7b, $67, $7f, $7f, $3f, 
    $3f, $3e, $3e, $1e, $1e, $08
  );
boat0_ls: array [0..29] of byte =
  (
    $04, $04, $0c, $0c, $1e, $1e, $1e, $3e, 
    $3f, $3f, $7f, $7f, $73, $63, $41, $5d, 
    $7d, $7d, $5d, $6f, $73, $7f, $7e, $7e, 
    $7e, $7e, $3e, $3c, $3c, $08
  );
boat0_r: array [0..29] of byte =
  (
    $00, $40, $60, $60, $70, $70, $78, $78, 
    $7c, $7c, $7e, $7e, $7f, $7f, $43, $41, 
    $5d, $5f, $5f, $3d, $3b, $27, $3f, $1f, 
    $1f, $1f, $1f, $0f, $0f, $0c
  );
boat0_rs: array [0..29] of byte =
  (
    $00, $01, $03, $07, $07, $0f, $0f, $1f, 
    $1f, $3f, $3f, $3f, $7f, $7f, $61, $41, 
    $5d, $7d, $7d, $5e, $ee, $f2, $fe, $fc, 
    $fc, $fc, $f8, $78, $78, $10
  );

boat1_s: array [0..29] of byte =
  (
    $00, $10, $20, $30, $24, $70, $60, $70,
    $60, $70, $62, $70, $78, $7c, $6e, $7e,
    $7e, $3a, $10, $44, $78, $72, $60, $70,
    $60, $70, $60, $34, $20, $30
  );
boat1_l: array [0..29] of byte =
  (
    $00, $20, $00, $20, $48, $60, $40, $64,
    $70, $e0, $f2, $e0, $f2, $fa, $fe, $ce,
    $78, $78, $74, $08, $f0, $f0, $60, $70,
    $60, $72, $78, $30, $38, $10
  );
boat1_ls: array [0..29] of byte =
  (
    $08, $00, $18, $14, $18, $30, $30, $22,
    $70, $60, $70, $e0, $78, $78, $7c, $64,
    $3e, $be, $de, $e4, $f8, $f0, $e2, $f0,
    $e0, $40, $60, $44, $60, $00
  );
boat1_r: array [0..29] of byte =
  (
    $00, $80, $80, $a0, $c0, $90, $c0, $88,
    $e0, $c4, $e0, $c2, $e4, $f4, $fa, $fe,
    $4c, $58, $78, $34, $09, $71, $31, $39,
    $31, $39, $11, $1d, $18, $08
  );
boat1_rs: array [0..29] of byte =
  (
    $00, $02, $04, $04, $0c, $08, $1c, $10,
    $38, $30, $78, $70, $78, $a0, $3c, $7c,
    $66, $b6, $be, $de, $e0, $f8, $e0, $c4,
    $e0, $c0, $88, $c0, $80, $10
  );

gates: array [0..7] of byte = 
(
	%10100000,
	%11110000,
	%11110000,
	%10100000,
	%10100000,
	%10100000,
	%10100000,
	%10100000
);

var boat0: array [0..4] of pointer = (@boat0_r, @boat0_l, @boat0_s, @boat0_ls, @boat0_rs);
var boat1: array [0..4] of pointer = (@boat1_r, @boat1_l, @boat1_s, @boat1_ls, @boat1_rs);



const
TILE_GROUND = $41;
TILE_WATER = $40;
TILE_END = $4e;
TILE_BL_S = $4544;
TILE_BL_R = $4746;
TILE_BL_L = $4948;
TILE_BR_S = $4342;
TILE_BR_R = $4B4A;
TILE_BR_L = $4D4C;

TILE_DAMAGE_OFF = $7e;
TILE_DAMAGE_ON = $7f;


var
BL_TILES: array [0..2] of word = (TILE_BL_L, TILE_BL_S, TILE_BL_R);
BR_TILES: array [0..2] of word = (TILE_BR_L, TILE_BR_S, TILE_BR_R);



