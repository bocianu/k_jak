unit dl;
interface
uses atari;

const
    DL_BLANK1 = 0;
    DL_BLANK2 = %00010000;
    DL_BLANK3 = %00100000;
    DL_BLANK4 = %00110000;
    DL_BLANK5 = %01000000;
    DL_BLANK6 = %01010000;
    DL_BLANK7 = %01100000;
    DL_BLANK8 = %01110000;

    DL_DLI = %10000000;
    DL_LMS = %01000000;
    DL_VSCROLL = %00100000;
    DL_HSCROLL = %00010000;

    DL_MODE_40x24T2 = 2;
    DL_MODE_40x24T5 = 4;
    DL_MODE_40x12T5 = 5;
    DL_MODE_20x24T5 = 6;
    DL_MODE_20x12T5 = 7;
    DL_MODE_40x24G4 = 8;
    DL_MODE_80x48G2 = 9;
    DL_MODE_80x48G4 = $A;
    DL_MODE_160x96G2 = $B;
    DL_MODE_160x192G2 = $C;
    DL_MODE_160x96G4 = $D;
    DL_MODE_160x192G4 = $E;
    DL_MODE_320x192G2 = $F;

    DL_JMP = %00000001;
    DL_JVB = %01000001;

var dl_addr:word;
	dl_ptr:byte;

procedure DL_Init(address:word);
procedure DL_Push(value:byte);overload;
procedure DL_Push(value, count:byte);overload;
procedure DL_Push(address:word);overload;
procedure DL_Push(value:byte; address:word);overload;
procedure DL_Start;

implementation

procedure DL_Init(address:word);
begin
	dl_addr := address;
	dl_ptr := 0;
end;

procedure DL_Push(value:byte);overload;
begin
	Poke(dl_addr+dl_ptr,value);
	Inc(dl_ptr);
end;	

procedure DL_Push(value, count:byte);overload;
begin
	while count>0 do begin
		DL_Push(value);
		Dec(count);
	end;
end;	

procedure DL_Push(address:word);overload;
begin
		DL_Push(Lo(address));
		DL_Push(Hi(address));
end;

procedure DL_Push(value:byte; address:word);overload;
begin
		DL_Push(value);
		DL_Push(address);
end;

	
procedure DL_Start;
begin
	SDLSTL := dl_addr;
end;	

end.
