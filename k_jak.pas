program k_jak;
uses crt, mypmg, atari, dl, joystick, rmt;

{$r assets.res}

const
	CHARGAME = $6800;
	PMGBASE = $5000;
	DL_RAM = $6000;
	TXT_RAM = $6200;
	GFX_RAM = $7000;
	TITLESCR = $7800;
	TITLEFNT = $8000;

	rmt_player = $a000;
    rmt_modul = $4200;
	
	RIVER_LEN = 1000;
	YOFF_MAX = 50;
	GATE_WIDTH = 5;
	GATE_WIDTH_PX = GATE_WIDTH shl 2;
	FINISH_OFFSET = 44;
	
	TURN_FORCE = 12;
	FRICTION = 5;
	MAX_TURN_SPEED = 500;
	MAX_SPEED =1000;
	ACCELERATION = 10;
	RIVER_SPEED = 200;
	HIT_SLOWDOWN = 50;
	BANK_BOUNCE = 2;
	DAMAGE_SPAN = 5;
	
var
	vscroll,vframe,f: byte;
	w, roffset: word;
	yoffset: array[0..YOFF_MAX] of word;
	bankl,bankr,banklprev,bankrprev:byte;
	river: array[0..RIVER_LEN] of byte;
	boatX, boatY, boatFrame, boatFramePrev: byte;
	joy: byte;
	cx,cy : word;
	sx,sy: smallInt;
	dx,dy: byte;
	gx,gy: byte;
	pgy: byte;
	gt: byte;
	finished:byte;
	damage:byte;
	damageWaitSpan:byte;
	infobar:Tstring = 'Czas: 0:00:00  K jak: '~;
	s:TString;
	timeM: byte;
	timeS: byte;
	timeF: byte;


    msx: TRMT;	

	
	
{$i sprites.inc}

procedure DliGame; interrupt;
begin
    asm {
        pha
        lda #$b0
		sta ATARI.wsync
		sta ATARI.colpf2
        pla
        };
end;

procedure InitDl;
begin
	DL_Init(DL_RAM);
	DL_Push(DL_BLANK8, 3);
	DL_Push(DL_MODE_40x24T5 + DL_LMS + DL_VSCROLL, GFX_RAM);
	DL_Push(DL_MODE_40x24T5 + DL_VSCROLL, 20);
	DL_Push(DL_MODE_40x24T5 );
	DL_Push(DL_BLANK2 + DL_DLI);
	DL_Push(DL_MODE_40x24T2 + DL_LMS, TXT_RAM);
	DL_Push(DL_MODE_40x24T2,2);
	DL_Push(DL_BLANK8);
	DL_Push(DL_JVB, DL_ADDR);
	// DL_Push();
	
	SetIntVec(iDli,@DliGame);
	nmien:=$c0;
	DL_Start;
	
end;

procedure setGfxRam(vframe:byte);
begin
   DPoke(DL_RAM+4,GFX_RAM+yoffset[vframe]);
end;

procedure setGate;
begin
	gy:=24-8;
	pgy:=202;
	gx:=52+(river[roffset+44] shl 2)+Random(byte((river[roffset+45]-GATE_WIDTH-1) shl 2));
	gt:=0;
	PMG_pcolr2_S:=$82;
	PMG_pcolr3_S:=$82;
end;

procedure drawGate;
var goff,gsize:byte;
begin
	fillByte(pointer(PMGBASE+768+pgy),12,0);
	goff:=0;
	gsize:=8;
	if gy<32 then begin 
		goff:=32-gy;
		if goff>8 then goff:=8;
		gsize:=8-goff;
	end;
	if gy>=192 then begin
		gsize:=201-gy;
		
		if gsize>200 then begin
			gx:=0;
			gsize:=8;
		end;
	end;
	Move(gates[goff],pointer(PMGBASE+768+gy+goff),gsize);
	hposm2:=gx;
	hposm3:=gx+(GATE_WIDTH shl 2);
	pgy:=gy;
end;

procedure timeZero;
begin
	timeF:=0;
	timeS:=0;
	timeM:=0;
end;

procedure timeTick;
begin
	inc(timeF);
	if timeF=50 then begin
		timeF:=0;
		inc(timeS);
		if timeS=60 then begin
			timeS:=0;
			inc(timeM);
		end;
	end;
end;

procedure timeAddSec(sec:byte);
begin
	inc(timeS,sec);
	if timeS>60 then begin
		timeS:=timeS-60;
		inc(timeM);
	end;
end;

procedure timeShow;
begin

    Str(timeF, s);
    asm {
		ldy	#1
		lda timeF
		cmp #10
		bpl @+
		lda #16
		sta TXT_RAM+52
		lda adr.s,y
        sub #32
        sta TXT_RAM+52,y
		jmp @+1
@       lda adr.s,y
        sub #32
        sta TXT_RAM+51,y
        iny
        dec adr.s
        bne @-
@
    };

    Str(timeS, s);
    asm {
		ldy	#1
		lda timeS
		cmp #10
		bpl @+
		lda #16
		sta TXT_RAM+49
		lda adr.s,y
        sub #32
        sta TXT_RAM+49,y
		jmp @+1
@       lda adr.s,y
        sub #32
        sta TXT_RAM+48,y
        iny
        dec adr.s
        bne @-
@
    };
    
    Str(timeM, s);
    asm {
		ldy	#1
@		lda adr.s,y
        sub #32
        sta TXT_RAM+46,y
        iny
        dec adr.s
        bne @-
     };
end;

procedure precalc;
var w: word;
  off,width:byte;
  r:shortInt;
begin
	for w:=0 to YOFF_MAX do begin
		yoffset[w]:=w*40;
	end;
	
	
	off:=10;
	width:=20;
	banklprev:=off;
	bankrprev:=off+width;
	w:=0;
	repeat
		river[w]:=off;
		river[w+1]:=width;
		r:=Random(3)-1;
		off:=off+r;
		if r=0 then begin 
			r:=Random(3)-1;
			width:=width+r;
    	end;
		if width>28 then width:=28;
		if width<16 then width:=16;
		if off<2 then off:=2;
		if off+width>38 then off:=38-width;
		Inc(w,2);
		// writeln(w);
	until w>=RIVER_LEN;
end;

procedure putRiverLine(v_off:byte;r_off:word);
var vmem:word;
	loff,roff:shortInt;
	tile:byte;
begin
	vmem:=GFX_RAM+yoffset[v_off];
	bankl:=river[r_off];
	bankr:=bankl+river[r_off+1];
	fillbyte(pointer(vmem),40,TILE_GROUND);
	tile:=TILE_WATER;
	if (r_off=RIVER_LEN-FINISH_OFFSET) then tile:=TILE_END;
	fillbyte(pointer(vmem+bankl),river[r_off+1],tile);
	loff:=banklprev-bankl;
	roff:=bankrprev-bankr;
	if loff<0 then loff:=0;
	if roff>0 then roff:=0;
	Dpoke(vmem+bankl+loff-1,BL_TILES[banklprev-bankl+1]);
	Dpoke(vmem+bankr+roff,BR_TILES[bankrprev-bankr+1]);
end;

procedure putRiverBlock(r_off: word;s_off: byte);
begin
	while (s_off>0) do begin
		putRiverLine(s_off,r_off);
		banklprev:=bankl;
		bankrprev:=bankr;
		Inc(r_off,2);
		dec(s_off);
	end;
end;

procedure setPlayerX;
begin
	PMG_hpos1:=boatX+1;
	PMG_hpos0:=boatX;
end;

procedure showDamageBar;
begin
	fillChar(pointer(TXT_RAM+63),16,TILE_DAMAGE_ON);
	fillChar(pointer(TXT_RAM+79-damage),damage,TILE_DAMAGE_OFF);
end;

procedure showPlayer(frame:byte);
begin
	move(boat0[frame],pointer(PMGBASE+1024+boatY),30);
	move(boat1[frame],pointer(PMGBASE+1024+256+boatY),30);
	setPlayerX;
end;


procedure showTitle;
begin
	asm {
	
	
fcnt	equ $f0;.ds 2
fadr	equ $f2;.ds 2
fhlp	equ $f4;.ds 2
cloc	equ $f6;.ds 1
regA	equ $f7;.ds 1
regX	equ $f8;.ds 1
regY	equ $f9;.ds 1

	sei			;stop IRQ interrupts
	mva #$00 nmien		;stop NMI interrupts
	sta dmactl
	mva #$fe portb		;switch off ROM to get 16k more ram

	mwa #NMI $fffa		;new NMI handler

	mva #$c0 nmien		;switch on NMI+DLI again

	ift CHANGES		;if label CHANGES defined

_lp	lda trig0		; FIRE #0
	beq stop

	lda trig1		; FIRE #1
	beq stop

	lda consol		; START
	and #1
	beq stop

	;lda skctl
	;and #$04
	bne _lp			;wait to press any key; here you can put any own routine

	els

null	jmp DLI.dli1		;CPU is busy here, so no more routines allowed

	eif


stop
	mva #$00 pmcntl		;PMG disabled
	tax
	sta:rne hposp0,x+

	mva #$ff portb		;ROM switch on
	mva #$40 nmien		;only NMI interrupts, DLI disabled
	cli			;IRQ enabled

	rts			;return to ... DOS

; ---	DLI PROGRAM

.local	DLI

	?old_dli = *

	ift !CHANGES

dli1	lda trig0		; FIRE #0
	beq stop

	lda trig1		; FIRE #1
	beq stop

	lda consol		; START
	and #1
	beq stop

	lda skctl
	and #$04
	beq stop

	lda vcount
	cmp #$02
	bne dli1

	:3 sta wsync

	DLINEW dli12

	eif

dli_start

dli12
	sta regA

	sta wsync		;line=24
	sta wsync		;line=25
c5	lda #$D8
	sta wsync		;line=26
	sta color3
	DLINEW DLI.dli2 1 0 0

dli2
	sta regA
	lda >TITLEFNT+$400*$01
	sta wsync		;line=40
	sta chbase
	sta wsync		;line=41
	sta wsync		;line=42
	sta wsync		;line=43
	sta wsync		;line=44
	sta wsync		;line=45
c6	lda #$98
	sta wsync		;line=46
	sta color1
	sta wsync		;line=47
	sta wsync		;line=48
	sta wsync		;line=49
	sta wsync		;line=50
c7	lda #$EC
	sta wsync		;line=51
	sta color3
	sta wsync		;line=52
c8	lda #$0E
	sta wsync		;line=53
	sta color3
	DLINEW dli3 1 0 0

dli3
	sta regA
	lda >TITLEFNT+$400*$02
	sta wsync		;line=64
	sta chbase
	DLINEW dli4 1 0 0

dli4
	sta regA
	lda >TITLEFNT+$400*$03
	sta wsync		;line=104
	sta chbase
	DLINEW dli5 1 0 0

dli5
	sta regA
	lda >TITLEFNT+$400*$04
	sta wsync		;line=136
	sta chbase
	DLINEW dli6 1 0 0

dli6
	sta regA
	lda >TITLEFNT+$400*$05
	sta wsync		;line=168
	sta chbase
	DLINEW dli7 1 0 0

dli7
	sta regA
	lda >TITLEFNT+$400*$06
	sta wsync		;line=200
	sta chbase
	DLINEW dli8 1 0 0

dli8
	sta regA
	lda >TITLEFNT+$400*$01
	sta wsync		;line=208
	sta chbase
	DLINEW dli9 1 0 0

dli9
	sta regA
	lda >TITLEFNT+$400*$00
	sta wsync		;line=216
	sta chbase

	lda regA
	rti

.endl

; ---

CHANGES = 1
FADECHR	= 0

; ---

.proc	NMI

	bit nmist
	bpl VBL

	jmp DLI.dli_start
dliv	equ *-2

VBL
	sta regA
	stx regX
	sty regY

	sta nmist		;reset NMI flag

	mwa #TITLESCR dlptr		;ANTIC address program

	mva #scr40 dmactl	;set new screen width

	inc cloc		;little timer

; Initial values

	lda >TITLEFNT+$400*$00
	sta chbase
c0	lda #$00
	sta colbak
	lda #$02
	sta chrctl
	lda #$04
	sta gtictl
c1	lda #$16
	sta color0
c2	lda #$9C
	sta color1
c3	lda #$FC
	sta color2
c4	lda #$0E
	sta color3
x0	lda #$00
	sta hposp0
	sta hposp1
	sta hposp2
	sta hposp3
	sta hposm0
	sta hposm1
	sta hposm2
	sta hposm3
	sta sizep0
	sta sizep1
	sta sizep2
	sta sizep3
	sta sizem
	sta colpm0
	sta colpm1
	sta colpm2
	sta colpm3

	mwa #DLI.dli_start dliv	;set the first address of DLI interrupt

;this area is for yours routines
	lda MSX
	ldy MSX+1

    jsr RMT.TRMT.PLAY


quit
	lda regA
	ldx regX
	ldy regY
	rti

.endp

.MACRO	DLINEW
	mva <:1 NMI.dliv
	ift [>?old_dli]<>[>:1]
	mva >:1 NMI.dliv+1
	eif

	ift :2
	lda regA
	eif

	ift :3
	ldx regX
	eif

	ift :4
	ldy regY
	eif

	rti

	.def ?old_dli = *
.ENDM

	
	
};

end;

begin
    msx.player := pointer(rmt_player);
    msx.modul := pointer(rmt_modul);
	
	msx.Init(0);
	showTitle;
	Randomize;
	Writeln('Please wait, precalculating...');
	precalc;
	
	repeat 
	if keypressed then readkey;
	chbas:=Hi(CHARGAME);
	color0:=$16;
	color1:=$1c;
	color2:=$98;
	boatX:=123;
	boatY:=160;

    PMG_Init(Hi(PMGBASE), PMG_sdmctl_oneline + PMG_sdmctl_default);
	PMG_gprior_S := PMG_overlap + 1;
	PMG_sizem := $00;
	PMG_Clear;
	FillByte(pointer(PMGBASE+384),128,$ff);
	//PMG_hposm2:=EDGE_L-16;
	//PMG_hposm3:=EDGE_R+16;
	//PMG_hposm0:=boatX-8;
	//PMG_hposm1:=boatY+8;	


	PMG_pcolr0_S:=$ea;
	PMG_pcolr1_S:=$e6;
	
	roffset:=0;
	vframe:=22;
	putRiverBlock(0,44);
	banklprev:=river[44];
	bankrprev:=banklprev+river[45];

	InitDl;
	setGfxRam(vframe);
	Move(@infobar[1],pointer(TXT_RAM+41),byte(infobar[0]));
	ShowDamageBar;
	
	vscroll:=7;
	
	sx:=0;
	sy:=RIVER_SPEED; 
	boatFrame:=2;
	boatFramePrev:=2;
	finished:=0;
	damage:=0;
	showDamageBar;
	TimeZero;
	
	showPlayer(boatFrame);
	msx.Init(5);
	repeat 
		
		Pause;
		
		if finished=0 then begin
			if (gx=0) and (roffset<RIVER_LEN-(FINISH_OFFSET shl 1)) then setGate
			else begin
				
				Inc(gy,dy);
				drawGate;
				
				if (gy>boatY+4) and (gt=0) then
					if (gx<=boatX+4) and (gx+GATE_WIDTH_PX>=boatX+4) then begin
						gt:=1;
						PMG_pcolr2_S:=$b6;
						PMG_pcolr3_S:=$b6;
					end else begin
						gt:=2;
						PMG_pcolr2_S:=$26;
						PMG_pcolr3_S:=$26;	
						timeAddSec(5);			   
					end;
					   
				
			end;
			
			Dec(vscroll,dy);
			if (vscroll>7) then begin
				vscroll:=vscroll and 7;
				Inc(roffset,2);
				if (roffset=RIVER_LEN) then begin
					roffset:=0
				end;
				Dec(vframe);
				if (vframe=1) then begin
					vframe:=23;
				end;
				putRiverLine(vframe,roffset+44);
				putRiverLine(vframe+22,roffset+44);
				banklprev:=bankl;
				bankrprev:=bankr;
				
				setGfxRam(vframe);
				// poke(TXT_RAM,gy);
			end;
			vscrol:=vscroll;
			
			joy:=(not stick0) and %00001111;
			if joy<>0 then begin
					if joy and 4<>0 then sx:=sx-TURN_FORCE;
					if joy and 8<>0 then sx:=sx+TURN_FORCE;
					if joy and 1<>0 then sy:=sy+ACCELERATION;
			end;
			sy:=sy-FRICTION;
			if sx > MAX_TURN_SPEED then sx:=MAX_TURN_SPEED;
			if sx < -MAX_TURN_SPEED then sx:=-MAX_TURN_SPEED;
			if sy > MAX_SPEED then sy:=MAX_SPEED;
			if sy < RIVER_SPEED then sy:=RIVER_SPEED;
							
			// count displacement
			cx:=cx+Abs(sx);
			cy:=cy+sy;
			dx:=Hi(cx);
			dy:=Hi(cy);
			cx:=Lo(cx);
			cy:=Lo(cy);
					
			f:=abs(sx) shr 5;
			if f>2 then f:=2;
					
			if sx>0 then begin 
				boatX:=boatX+dx;
				sx:=sx-FRICTION;
				if sx<0 then sx:=0;
				boatFrame:=2+f;
			end;
			
			if sx<0 then begin 
				boatX:=boatX-dx;
				sx:=sx+FRICTION;
				if sx>0 then sx:=0;
				boatFrame:=2-f;
			end;
			
			
			if boatFrame<>boatFramePrev then begin
				showPlayer(boatFrame);
				boatFramePrev:=boatFrame;
			end;
				
			
			setPlayerX;
			
			if (hposm0 and 3 <> 0) or (hposm1 and 3 <> 0) then begin
				if ((river[roffset+6] shl 2)+64)<boatX then begin
					boatX:=boatX-BANK_BOUNCE;
				end else begin
					boatX:=boatX+BANK_BOUNCE;
				end;
				sy:=sy-HIT_SLOWDOWN;
				
				if damageWaitSpan=0 then begin
					Inc(damage);
					if damage>=16 then begin 
						damage:=16;
						finished:=2;
					end;
					showDamageBar;
					damageWaitSpan:=DAMAGE_SPAN;
				end;
			end;

			if damageWaitSpan>0 then Dec(damageWaitSpan);

			if roffset+6>RIVER_LEN-FINISH_OFFSET then begin
				finished:=1;
			end;
			
			hitclr:=$ff;
				
			timeTick;
		
		end; // end do in game loop

		timeShow;

		msx.Play;
				
	until (finished <> 0) and keypressed;
	
	if keypressed then readkey;
	msx.Init(0);
	showTitle;
	until false;

end.
